#include <iostream>>
#include <string>

using namespace std;

struct T_Kunde {
	int ID;
	string vName;
	string nName;
	string tNummer;
};

//Prototypen
T_Kunde* Hinzufuegen(T_Kunde* tKunden, int& iFeldG, int& iIDC);
void Suchen(T_Kunde tKunden[], int iFeldG);
void Loeschen(T_Kunde tKunden[], int iFeldG);
T_Kunde* ExtendArray(T_Kunde* piOld, int& Length, T_Kunde Attachment);
//Sortieren
void SortierenArr(T_Kunde* tStKunden, int Length);
void vertauschen(T_Kunde &iErste, T_Kunde &iZweite);
void sortierenIDauf(T_Kunde &iErstere, T_Kunde &iZweitere, T_Kunde &iDritterste);
void sortierenIDab(T_Kunde &iErstere, T_Kunde &iZweitere, T_Kunde &iDritterste);
void sortierenNNauf(T_Kunde &iErstere, T_Kunde &iZweitere, T_Kunde &iDritterste);
void sortierenNNab(T_Kunde &iErstere, T_Kunde &iZweitere, T_Kunde &iDritterste);

int main(void)
{
	char cEingabe = ' ';
	int iArrLen = 0;
	bool bBeenden = false;
	int iIDCount = 0;
	T_Kunde* Kunden;
	Kunden = nullptr;
	do
	{
		system("cls");
		cEingabe = ' ';
		cout << "NETTMANN\n\n_____________________________\n\nKundenverwaltung\n\n_____________________________\nBitte waehlen Sie:\n1.\tKunden anzeigen\n2.\tKunde hinzufuegen\n3.\tKunde suchen\n4.\tLoeschen\n5.\tBeenden\n\nIhre Auswahl: ";
		cin >> cEingabe;

		switch (cEingabe)
		{
		case '1': SortierenArr(Kunden, iArrLen); //Anzeigen
			break;
		case '2': Kunden = Hinzufuegen(Kunden, iArrLen, iIDCount);
			break;
		case '3': Suchen(Kunden, iArrLen);
			break;
		case '4': Loeschen(Kunden, iArrLen);
			break;
		case '5': bBeenden = true;
			break;
		default:
			break;
		}
		getchar();
		getchar();
	} while (bBeenden == false);

	return 0;
}


T_Kunde* Hinzufuegen(T_Kunde* tKunden, int& iFeldG, int& iIDC)
{
	T_Kunde NeuerKunde;
	NeuerKunde.ID = 0;
	NeuerKunde.vName = "";
	NeuerKunde.nName = "";
	NeuerKunde.tNummer = "";
	
			iIDC++;
			NeuerKunde.ID = iIDC;
			cout << "\nVorname:"<<endl;
			cin >> NeuerKunde.vName;
			cout << "\nNachname:" << endl;
			cin >> NeuerKunde.nName;
			cout << "\nNummer:" << endl;
			cin >> NeuerKunde.tNummer;

			tKunden = ExtendArray(tKunden, iFeldG, NeuerKunde);
			cout << "\nKunde angelegt!";

	return tKunden;
}


void Suchen(T_Kunde tKunden[], int iFeldG)
{
	bool bGefunden = false;
	string strSuche = "";
	cout << "\nBitte Vor- oder Nachnamen eingeben:" << endl;
	cin >> strSuche;

	for (int i = 0;i < iFeldG;i++)
	{
		if (tKunden[i].vName == strSuche || tKunden[i].nName == strSuche)
		{
			cout <<"\n" << tKunden[i].ID << " | " << tKunden[i].vName << " | " << tKunden[i].nName << " | " << tKunden[i].tNummer << endl;
			i = iFeldG;
			bGefunden = true;
		}
	}
	
	if (bGefunden == false)
	{
		cout << "\nKeinen Eintrag gefunden!" << endl;
	}
}

void Loeschen(T_Kunde tKunden[], int iFeldG)
{
	bool bGefunden = false;
	char cLoesch = ' ';
	string strSuche = "";

	cout << "\nBitte Vor- oder Nachnamen eingeben:" << endl;
	cin >> strSuche;

	for (int i = 0; i < iFeldG; i++)
	{
		if (tKunden[i].vName == strSuche || tKunden[i].nName == strSuche)
		{
			cout << "\n" << tKunden[i].ID << " | " << tKunden[i].vName << " | " << tKunden[i].nName << " | " << tKunden[i].tNummer<< "\nloeschen?(j/J)" << endl;
			cin >> cLoesch;
			if (cLoesch == 'j' || cLoesch == 'J')
			{
				tKunden[i].ID = 0;
				tKunden[i].vName = "";
				tKunden[i].nName = "";
				tKunden[i].tNummer = "";
				cout << "Geloscht!";
			}

			i = iFeldG;
			bGefunden = true;
		}
	}

	if (bGefunden == false)
	{
		cout << "\nKeinen Eintrag gefunden!" << endl;
	}
}

T_Kunde* ExtendArray(T_Kunde* piOld, int& Length, T_Kunde Attachment)
{
	T_Kunde* piNew;

	piNew = new(T_Kunde[Length + 1]);

	for (int i = 0; i < Length; i++)
	{
		piNew[i] = piOld[i];
	}

	piNew[Length] = Attachment;
	Length++;

	delete[] (piOld);
	piOld = nullptr;
	
	return piNew;
}



void SortierenArr(T_Kunde* tStKunden, int Length)
{
	char cSortEingabe = ' ';
	cout << "Anzeigen als:\n\nAufsteigend sortiert (ID) (1)\nAbsteigend sortiert (ID)(2)\nAufsteigend sortiert (Nachname) (3)\nAbsteigend sortiert (Nachname)(4)"<< endl;
	cin >> cSortEingabe;
	for (int ii = 0; ii < Length; ii++)
	{
		for (int i = 0; i < Length - 2; i++)
		{
			switch (cSortEingabe) {
			case '1': sortierenIDauf(tStKunden[i], tStKunden[i + 1], tStKunden[i + 2]); break;
			case '2': sortierenIDab(tStKunden[i], tStKunden[i + 1], tStKunden[i + 2]); break;
			case '3': sortierenNNauf(tStKunden[i], tStKunden[i + 1], tStKunden[i + 2]); break;
			case '4': sortierenNNab(tStKunden[i], tStKunden[i + 1], tStKunden[i + 2]); break;
			default: break;
			}
		}
	}

	cout << "\n" << Length << " Eintraege\nID | Vorname | Nachname | Nummer\n";

	for (int i = 0; i < Length; i++)
	{
		if (tStKunden[i].ID != 0 && tStKunden[i].vName != "" && tStKunden[i].nName != "" && tStKunden[i].tNummer != "")
		{
			cout << tStKunden[i].ID << " | " << tStKunden[i].vName << " | " << tStKunden[i].nName << " | " << tStKunden[i].tNummer << endl;
		}
	}
}

void vertauschen(T_Kunde &iErste, T_Kunde &iZweite)
{
	T_Kunde iZwischen;
	iZwischen = iErste;
	iErste = iZweite;
	iZweite = iZwischen;
}

void sortierenIDauf(T_Kunde &iErstere, T_Kunde &iZweitere, T_Kunde &iDritterste)
{
	if (iZweitere.ID > iDritterste.ID) {
		vertauschen(iZweitere, iDritterste);
	}
		if (iErstere.ID > iZweitere.ID) {
			vertauschen(iErstere, iZweitere);
			if (iZweitere.ID > iDritterste.ID) {
				vertauschen(iZweitere, iDritterste);
			}
		}	
}

void sortierenIDab(T_Kunde &iErstere, T_Kunde &iZweitere, T_Kunde &iDritterste)
{
	if (iZweitere.ID < iDritterste.ID) {
		vertauschen(iZweitere, iDritterste);
	}
	if (iErstere.ID < iZweitere.ID) {
		vertauschen(iErstere, iZweitere);
		if (iZweitere.ID < iDritterste.ID) {
			vertauschen(iZweitere, iDritterste);
		}
	}
}

void sortierenNNauf(T_Kunde &iErstere, T_Kunde &iZweitere, T_Kunde &iDritterste)
{	
	if (iZweitere.nName[0] != iDritterste.nName[0])
	{
		if (iZweitere.nName[0] > iDritterste.nName[0]) 
		{
			vertauschen(iZweitere, iDritterste);
		}
	}
	else
	{
		if (iZweitere.nName[1] > iDritterste.nName[1]) 
		{
			vertauschen(iZweitere, iDritterste);
		}
	}
	
	if (iErstere.nName[0] != iZweitere.nName[0])
	{
		if (iErstere.nName[0] > iZweitere.nName[0]) 
		{
			vertauschen(iErstere, iZweitere);


			if (iZweitere.nName[0] != iDritterste.nName[0])
			{
				if (iZweitere.nName[0] > iDritterste.nName[0]) 
				{
					vertauschen(iZweitere, iDritterste);
				}
			}
			else
			{
				if (iZweitere.nName[1] > iDritterste.nName[1]) 
				{
					vertauschen(iZweitere, iDritterste);
				}
			}
		}
	}
	else
	{
		if (iErstere.nName[1] > iZweitere.nName[1])
		{
			vertauschen(iErstere, iZweitere);


			if (iZweitere.nName[0] != iDritterste.nName[0])
			{
				if (iZweitere.nName[0] > iDritterste.nName[0])
				{
					vertauschen(iZweitere, iDritterste);
				}
			}
			else
			{
				if (iZweitere.nName[1] > iDritterste.nName[1])
				{
					vertauschen(iZweitere, iDritterste);
				}
			}
		}
	}
}



void sortierenNNab(T_Kunde &iErstere, T_Kunde &iZweitere, T_Kunde &iDritterste)
{
	if (iZweitere.nName[0] < iDritterste.nName[0]) {
		vertauschen(iZweitere, iDritterste);
	}
	if (iErstere.nName[0] < iZweitere.nName[0]) {
		vertauschen(iErstere, iZweitere);
		if (iZweitere.nName[0] < iDritterste.nName[0]) {
			vertauschen(iZweitere, iDritterste);
		}
	}
}